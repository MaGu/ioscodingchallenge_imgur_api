//
//  ViewController.m
//  iOSCodingChallenge
//
//  Created by MaGu on 3/18/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import "ViewController.h"
#import "ImgurTableViewCell.h"
#import "PersistencyManager.h"
#import "Constants.h"
#import "DetailViewController.h"
#import "ImgurObject.h"

#import "FileUtilities.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>
{
    NSMutableArray *arrayResultSearch;
    NSInteger index;
    NSString *searchTitle;
    int searchPage;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    //NSLog(@"Path Files: %@", [FileUtilities documentDirectory]);
    
    [super viewDidLoad];
    self.title = @"Imgur´s API";
    [_activityIndicator setHidden:YES];
    arrayResultSearch = [[NSMutableArray alloc] init];
    searchTitle = @"";
    searchPage = 1;
    
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = btnBack;
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(tapAnywhere:)];
    [tapRecognizer setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tapRecognizer];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


#pragma mark - Keyboard Handler

- (void)tapAnywhere:(UITapGestureRecognizer *)tap
{
    [self.view endEditing:YES];
}


#pragma mark - UITableView Data Source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ImgurTableViewCell *cell = [tableView dequeueReusableCellWithClass:[ImgurTableViewCell class]];
    [cell configureWithImgurObject:arrayResultSearch[indexPath.row]];
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayResultSearch count];
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    index = indexPath.row;
    
    [self performSegueWithIdentifier:@"showDetail" sender:self];
}

#pragma mark - Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"])
    {
        DetailViewController *detailVC = [segue destinationViewController];
        detailVC.imageInfo = [(ImgurObject *)arrayResultSearch[index] getImageInfo];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row > (arrayResultSearch.count - 3))
    {
        searchPage ++;
        _tableViewResults.userInteractionEnabled = FALSE;
        [self beginRequestImagesWithText:searchTitle];
    }
}

#pragma mark - UISearchBar Delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    if (searchBar.text.length > 0)
    {
        if ([searchBar.text isEqualToString:searchTitle] || [searchTitle isEqualToString:@""])
        {
            searchTitle = searchBar.text;
            [self beginRequestImagesWithText:searchTitle];
        }
        else
        {
            [arrayResultSearch removeAllObjects];
            [_tableViewResults reloadData];
            searchTitle = searchBar.text;
            [self beginRequestImagesWithText:searchTitle];
        }
    }
}

- (void)reloadTableWithArray:(NSArray *)arrayImages
{
    _tableViewResults.userInteractionEnabled = YES;
    [_activityIndicator stopAnimating];
    [_activityIndicator setHidden:YES];
    [arrayResultSearch addObjectsFromArray:arrayImages];
    [_tableViewResults reloadData];
}

- (void)beginRequestImagesWithText:(NSString *)searchTitle
{
    [_activityIndicator startAnimating];
    [_activityIndicator setHidden:NO];
    
    PersistencyManager *persistency = [[PersistencyManager alloc] init];
    [persistency getImagesWithName:searchTitle withPage:searchPage withCompletion:^(NSArray *arrayImages, NSString *errorMessage, int errorCode)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             if (errorCode == kSUCCESS_CODE)
             {
                 [self reloadTableWithArray:arrayImages];
             }
             else
             {
                 UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Warning - %d", errorCode]
                                                                                message:@"An error occurred when consulting the images, please try it later"
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                       handler:^(UIAlertAction * action) {}];
                 
                 [alert addAction:defaultAction];
                 [self presentViewController:alert animated:YES completion:nil];
             }
         });
     }];
}


@end
