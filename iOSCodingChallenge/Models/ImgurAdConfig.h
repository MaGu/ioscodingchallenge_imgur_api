//
//  ImgurAdConfig.h
//  iOSCodingChallenge
//
//  Created by MaGu on 3/18/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImgurAdConfig : NSObject

@property (nonatomic, strong) NSArray *safeFlags;
@property (nonatomic, strong) NSArray *highRiskFlags;
@property (nonatomic, strong) NSArray *unsafeFlags;
@property (nonatomic) BOOL showsAds;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END
