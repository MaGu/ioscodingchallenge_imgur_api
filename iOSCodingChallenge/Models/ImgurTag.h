//
//  ImgurTag.h
//  iOSCodingChallenge
//
//  Created by MaGu on 3/18/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImgurTag : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *display_name;
@property (nonatomic) NSInteger followers;
@property (nonatomic) NSInteger total_items;
@property (nonatomic) BOOL following;
@property (nonatomic, strong) NSString *background_hash;
@property (nonatomic, strong) NSString *thumbnail_hash;
@property (nonatomic, strong) NSString *accent;
@property (nonatomic) BOOL background_is_animated;
@property (nonatomic) BOOL thumbnail_is_animated;
@property (nonatomic) BOOL is_promoted;
@property (nonatomic, strong) NSString *descriptionTag;
@property (nonatomic, strong) NSString *logo_hash;
@property (nonatomic, strong) NSString *logo_destination_url;
@property (nonatomic, strong) NSDictionary *description_annotations;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END
