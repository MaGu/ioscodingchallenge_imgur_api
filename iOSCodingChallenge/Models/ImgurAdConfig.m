//
//  ImgurAdConfig.m
//  iOSCodingChallenge
//
//  Created by MaGu on 3/18/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import "ImgurAdConfig.h"
#import "Constants.h"

@implementation ImgurAdConfig

- (instancetype)init
{
    if ((self = [super init]))
    {
        _safeFlags = nil;
        _highRiskFlags = nil;
        _unsafeFlags = nil;
        _showsAds = NO;
    }
    
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    if ((self = [super init]))
    {
        _safeFlags = [self getArrayObject:@"safeFlags" fromDictionary:dictionary];
        _highRiskFlags = [self getArrayObject:@"highRiskFlags" fromDictionary:dictionary];
        _unsafeFlags = [self getArrayObject:@"unsafeFlags" fromDictionary:dictionary];
        _showsAds = dictionary[@"showsAds"] ? [dictionary[@"showsAds"] boolValue] : NO;
    }
    
    return self;
}

- (NSArray *)getArrayObject:(NSString *)objectName fromDictionary:(NSDictionary *)dictionary
{
    NSArray *arrayAux = [dictionary isValid:objectName] ? dictionary[objectName] : nil;
    if (arrayAux != nil && arrayAux.count > 0)
    {
        return arrayAux;
    }
    
    return nil;
}

@end
