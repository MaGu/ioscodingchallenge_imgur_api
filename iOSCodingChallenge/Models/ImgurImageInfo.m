//
//  ImgurImageInfo.m
//  iOSCodingChallenge
//
//  Created by MaGu on 3/20/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import "ImgurImageInfo.h"

@implementation ImgurImageInfo

- (instancetype)init
{
    if ((self = [super init]))
    {
        _imageURL = nil;
        _imageID = @"";
        _title = @"";
    }
    
    return self;
}

@end
