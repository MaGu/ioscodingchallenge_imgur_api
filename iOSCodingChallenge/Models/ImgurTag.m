//
//  ImgurTag.m
//  iOSCodingChallenge
//
//  Created by MaGu on 3/18/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import "ImgurTag.h"
#import "Constants.h"

@implementation ImgurTag

- (instancetype)init
{
    if ((self = [super init]))
    {
        _name = @"";
        _display_name = @"";
        _followers = 0;
        _total_items = 0;
        _following = NO;
        _background_hash = @"";
        _thumbnail_hash = @"";
        _accent = @"";
        _background_is_animated = NO;
        _thumbnail_is_animated = NO;
        _is_promoted = NO;
        _descriptionTag = @"";
        _logo_hash = @"";
        _logo_destination_url = @"";
        _description_annotations = nil;
    }
    
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    if ((self = [super init]))
    {
        _name = [dictionary isValid:@"name"] ? [[NSString alloc] initWithString:dictionary[@"name"]] : @"";
        _display_name = [dictionary isValid:@"display_name"] ? [[NSString alloc] initWithString:dictionary[@"display_name"]] : @"";
        _followers = [dictionary isValid:@"followers"] ? [dictionary[@"followers"] integerValue] : 0;
        _total_items = [dictionary isValid:@"total_items"] ? [dictionary[@"total_items"] integerValue] : 0;
        _following = [dictionary isValid:@"following"] ? [dictionary[@"following"] boolValue] : NO;
        _background_hash = [dictionary isValid:@"background_hash"] ? [[NSString alloc] initWithString:dictionary[@"background_hash"]] : @"";
        _thumbnail_hash = [dictionary isValid:@"thumbnail_hash"] ? [[NSString alloc] initWithString:dictionary[@"thumbnail_hash"]] : @"";
        _accent = [dictionary isValid:@"accent"] ? [[NSString alloc] initWithString:dictionary[@"accent"]] : @"";
        _background_is_animated = [dictionary isValid:@"background_is_animated"] ? [dictionary[@"background_is_animated"] boolValue] : NO;
        _thumbnail_is_animated = [dictionary isValid:@"thumbnail_is_animated"] ? [dictionary[@"thumbnail_is_animated"] boolValue] : NO;
        _is_promoted = [dictionary isValid:@"is_promoted"] ? [dictionary[@"is_promoted"] boolValue] : NO;
        _descriptionTag = [dictionary isValid:@"description"] ? [[NSString alloc] initWithString:dictionary[@"description"]] : @"";
        _logo_hash = [dictionary isValid:@"logo_hash"] ? [[NSString alloc] initWithString:dictionary[@"logo_hash"]] : @"";
        _logo_destination_url = [dictionary isValid:@"logo_destination_url"] ? [[NSString alloc] initWithString:dictionary[@"logo_destination_url"]] : @"";
        _description_annotations = [dictionary isValid:@"description_annotations"] ? [[NSDictionary alloc] initWithDictionary:dictionary[@"description_annotations"]] : nil;
    }
    
    return self;
}

@end
