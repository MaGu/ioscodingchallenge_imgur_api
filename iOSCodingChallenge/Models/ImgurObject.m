//
//  ImgurObject.m
//  iOSCodingChallenge
//
//  Created by MaGu on 3/18/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import "ImgurObject.h"
#import "ImgurTag.h"
#import "ImgurImage.h"
#import "ImgurAdConfig.h"
#import "ImgurImageInfo.h"
#import "Constants.h"

@implementation ImgurObject

- (instancetype)init
{
    if ((self = [super init]))
    {
        _ID = @"";
        _title = @"";
        _descriptionImgurImage = @"";
        _datetime = @"";
        _cover = @"";
        _cover_width = 0;
        _cover_height = 0;
        _account_url = @"";
        _account_id = @"";
        _privacy = @"";
        _layout = @"";
        _views = 0;
        _link = @"";
        _ups = 0;
        _downs = 0;
        _points = 0;
        _score = 0;
        _is_album = NO;
        _vote = @"";
        _favorite = NO;
        _nsfw = NO;
        _section = @"";
        _comment_count = 0;
        _favorite_count = 0;
        _topic = @"";
        _topic_id = 0;
        _images_count = 0;
        _in_gallery = NO;
        _is_ad = NO;
        _tags = nil;
        _ad_type = 0;
        _ad_url = @"";
        _in_most_viral = NO;
        _include_album_ads = NO;
        _images = nil;
        _ad_config = nil;
    }
    
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    if ((self = [super init]))
    {
        _ID = [dictionary isValid:@"id"] ? [[NSString alloc] initWithString:dictionary[@"id"]] : @"";
        _title = [dictionary isValid:@"title"] ? [[NSString alloc] initWithString:dictionary[@"title"]] : @"";
        _descriptionImgurImage = [dictionary isValid:@"description"] ? [[NSString alloc] initWithString:dictionary[@"description"]] : @"";
        _datetime = [dictionary isValid:@"datetime"] ? [[NSString alloc] initWithString:dictionary[@"datetime"]] : @"";
        _cover = [dictionary isValid:@"cover"] ? [[NSString alloc] initWithString:dictionary[@"cover"]] : @"";
        _cover_width = [dictionary isValid:@"cover_width"] ? [dictionary[@"cover_width"] integerValue] : 0;
        _cover_height = [dictionary isValid:@"cover_height"] ? [dictionary[@"cover_height"] integerValue] : 0;
        _account_url = [dictionary isValid:@"account_url"] ? [[NSString alloc] initWithString:dictionary[@"account_url"]] : @"";
        _account_id = [dictionary isValid:@"account_id"] ? [[NSString alloc] initWithString:dictionary[@"account_id"]] : @"";
        _privacy = [dictionary isValid:@"privacy"] ? [[NSString alloc] initWithString:dictionary[@"privacy"]] : @"";
        _layout = [dictionary isValid:@"layout"] ? [[NSString alloc] initWithString:dictionary[@"layout"]] : @"";
        _views = [dictionary isValid:@"views"] ? [dictionary[@"views"] integerValue] : 0;
        _link = [dictionary isValid:@"link"] ? [[NSString alloc] initWithString:dictionary[@"link"]] : @"";
        _ups = [dictionary isValid:@"ups"] ? [dictionary[@"ups"] integerValue] : 0;
        _downs = [dictionary isValid:@"downs"] ? [dictionary[@"downs"] integerValue] : 0;
        _points = [dictionary isValid:@"points"] ? [dictionary[@"points"] integerValue] : 0;
        _score = [dictionary isValid:@"score"] ? [dictionary[@"score"] integerValue] : 0;
        _is_album = [dictionary isValid:@"is_album"] ? [dictionary[@"is_album"] boolValue] : NO;
        _vote = [dictionary isValid:@"vote"] ? [[NSString alloc] initWithString:dictionary[@"vote"]] : @"";
        _favorite = [dictionary isValid:@"favorite"] ? [dictionary[@"favorite"] boolValue] : NO;
        _nsfw = [dictionary isValid:@"nsfw"] ? [dictionary[@"nsfw"] boolValue] : NO;
        _section = [dictionary isValid:@"section"] ? [[NSString alloc] initWithString:dictionary[@"section"]] : @"";
        _comment_count = [dictionary isValid:@"comment_count"] ? [dictionary[@"comment_count"] integerValue] : 0;
        _favorite_count = [dictionary isValid:@"favorite_count"] ? [dictionary[@"favorite_count"] integerValue] : 0;
        _topic = [dictionary isValid:@"topic"] ? [[NSString alloc] initWithString:dictionary[@"topic"]] : @"";
        _topic_id = [dictionary isValid:@"topic_id"] ? [dictionary[@"topic_id"] integerValue] : 0;
        _images_count = [dictionary isValid:@"images_count"] ? [dictionary[@"images_count"] integerValue] : 0;
        _in_gallery = [dictionary isValid:@"in_gallery"] ? [dictionary[@"in_gallery"] boolValue] : NO;
        _is_ad = [dictionary isValid:@"is_ad"] ? [dictionary[@"is_ad"] boolValue] : NO;
        _ad_type = [dictionary isValid:@"ad_type"] ? [dictionary[@"ad_type"] integerValue] : 0;
        _ad_url = [dictionary isValid:@"ad_url"] ? [[NSString alloc] initWithString:dictionary[@"ad_url"]] : @"";
        _in_most_viral = [dictionary isValid:@"in_most_viral"] ? [dictionary[@"in_most_viral"] boolValue] : NO;
        _include_album_ads = [dictionary isValid:@"include_album_ads"] ? [dictionary[@"include_album_ads"] boolValue] : NO;
        _ad_config = [dictionary isValid:@"ad_config"] ? [[ImgurAdConfig alloc] initWithDictionary:dictionary[@"ad_config"]] : nil;
        _tags = nil;
        _images = nil;
        
        NSArray *arrayAux = [dictionary isValid:@"tags"] ? [NSArray arrayWithArray:dictionary[@"tags"]] : nil;
        if (arrayAux.count > 0)
        {
            NSMutableArray *arrayTags = [[NSMutableArray alloc] init];
            
            for (NSDictionary *dictTag in arrayAux)
            {
                ImgurTag *imgurTag = [[ImgurTag alloc] initWithDictionary:dictTag];
                [arrayTags addObject:imgurTag];
            }
            
            _tags = arrayTags.count ? [[NSArray alloc] initWithArray:arrayTags] : nil;
        }
        
        arrayAux = [dictionary isValid:@"images"] ? [NSArray arrayWithArray:dictionary[@"images"]] : nil;
        if (arrayAux.count > 0)
        {
            NSMutableArray *arrayImages = [[NSMutableArray alloc] init];
            
            for (NSDictionary *dictImage in arrayAux)
            {
                ImgurImage *imgurImage = [[ImgurImage alloc] initWithDictionary:dictImage];
                [arrayImages addObject:imgurImage];
            }
            
            _images = arrayImages.count ? [[NSArray alloc] initWithArray:arrayImages] : nil;
        }
    }
    
    return self;
}

- (ImgurImageInfo *)getImageInfo
{
    ImgurImageInfo *imageInfo = nil;
    
    if (_images.count > 0)
    {
        for (ImgurImage *image in _images)
        {
            if (image.link.length > 0)
            {
                imageInfo = [[ImgurImageInfo alloc] init];
                imageInfo.imageURL = [NSURL URLWithString:image.link];
                imageInfo.imageID = [NSString stringWithString:image.ID];
                imageInfo.title = [NSString stringWithString:_title];
                break;
            }
        }
    }
    else
    {
        if (_link.length > 0)
        {
            imageInfo = [[ImgurImageInfo alloc] init];
            imageInfo.imageURL = [NSURL URLWithString:_link];
            imageInfo.imageID = [NSString stringWithString:_ID];
            imageInfo.title = [NSString stringWithString:_title];
        }
    }
    
    return imageInfo;
}

- (NSString *)description
{
    NSMutableString *strDescription = [[NSMutableString alloc] init];
    [strDescription appendString:@"-- IMGUR OBJECT -- \n"];
    [strDescription appendFormat:@"ID: %@ \n", _ID];
    [strDescription appendFormat:@"Title: %@ \n", _title];
    [strDescription appendFormat:@"Description: %@ \n", _descriptionImgurImage];
    [strDescription appendFormat:@"Data Time: %@ \n", _datetime];
    [strDescription appendFormat:@"Cover: %@ \n", _cover];
    [strDescription appendFormat:@"Cover Width: %.02f \n", (float)_cover_width];
    [strDescription appendFormat:@"Cover Height: %.02f \n", (float)_cover_height];
    [strDescription appendFormat:@"Account URL: %@ \n", _account_url];
    [strDescription appendFormat:@"Account ID: %@ \n", _account_id];
    [strDescription appendFormat:@"Privacy: %@ \n", _privacy];
    [strDescription appendFormat:@"Layout: %@ \n", _layout];
    [strDescription appendFormat:@"Views: %.02f \n", (float)_views];
    [strDescription appendFormat:@"Link: %@ \n", _link];
    [strDescription appendFormat:@"Ups: %0.2f \n", (float)_ups];
    [strDescription appendFormat:@"Downs: %.02f \n", (float)_downs];
    [strDescription appendFormat:@"Points: %.02f \n", (float)_points];
    [strDescription appendFormat:@"Score: %.02f \n", (float)_score];
    [strDescription appendFormat:@"is Album: %@ \n", _is_album ? @"YES" : @"NO"];
    [strDescription appendFormat:@"Vote: %@ \n", _vote];
    [strDescription appendFormat:@"is Favorite: %@ \n", _favorite ? @"YES" : @"NO"];
    [strDescription appendFormat:@"is NSFW: %@ \n", _nsfw ? @"YES" : @"NO"];
    [strDescription appendFormat:@"Section: %@ \n", _section];
    [strDescription appendFormat:@"Comment count: %.02f \n", (float)_comment_count];
    [strDescription appendFormat:@"Favorite count: %.02f \n", (float)_favorite_count];
    [strDescription appendFormat:@"Topic: %@ \n", _topic];
    [strDescription appendFormat:@"Topic ID: %.02f \n", (float)_topic_id];
    [strDescription appendFormat:@"Images Count: %.02f \n", (float)_images_count];
    [strDescription appendFormat:@"In Gallery: %@ \n", _in_gallery ? @"YES" : @"NO"];
    return strDescription;
}

@end
