//
//  ImgurImage.m
//  iOSCodingChallenge
//
//  Created by MaGu on 3/18/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import "ImgurImage.h"
#import "Constants.h"

@implementation ImgurImage

- (instancetype)init
{
    if ((self = [super init]))
    {
        _ID = @"";
        _title = @"";
        _descriptionImage = @"";
        _datetime = @"";
        _type = @"";
        _animated = NO;
        _width = 0;
        _height = 0;
        _size = 0;
        _views = 0;
        _bandwidth = @"";
        _vote = @"";
        _favorite = NO;
        _nsfw = @"";
        _section = @"";
        _account_url = @"";
        _account_id = @"";
        _is_ad = NO;
        _in_most_viral = NO;
        _has_sound = NO;
        _tags = nil;
        _ad_type = 0;
        _ad_url = @"";
        _edited = @"";
        _in_gallery = NO;
        _link = @"";
        _mp4_size = 0;
        _mp4 = @"";
        _gifv = @"";
        _hls = @"";
        _processing = nil;
        _comment_count = @"";
        _favorite_count = @"";
        _ups = @"";
        _downs = @"";
        _points = @"";
        _score = @"";
    }
    
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    if ((self = [super init]))
    {
        _ID = [dictionary isValid:@"id"] ? [[NSString alloc] initWithString:dictionary[@"id"]] : @"";
        _title = [dictionary isValid:@"title"] ? [[NSString alloc] initWithString:dictionary[@"title"]] : @"";
        _descriptionImage = [dictionary isValid:@"description"] ? [[NSString alloc] initWithString:dictionary[@"description"]] : @"";
        _datetime = [dictionary isValid:@"datetime"] ? [[NSString alloc] initWithString:dictionary[@"datetime"]] : @"";
        _type = [dictionary isValid:@"type"] ? [[NSString alloc] initWithString:dictionary[@"type"]] : @"";
        _animated = [dictionary isValid:@"animated"] ? [dictionary[@"animated"] boolValue] : NO;
        _width = [dictionary isValid:@"width"] ? [dictionary[@"width"] integerValue] : 0;
        _height = [dictionary isValid:@"height"] ? [dictionary[@"height"] integerValue] : 0;
        _size = [dictionary isValid:@"size"] ? [dictionary[@"size"] integerValue] : 0;
        _views = [dictionary isValid:@"views"] ? [dictionary[@"views"] integerValue] : 0;
        _bandwidth = [dictionary isValid:@"bandwidth"] ? [[NSString alloc] initWithString:dictionary[@"bandwidth"]] : @"";
        _vote = [dictionary isValid:@"vote"] ? [[NSString alloc] initWithString:dictionary[@"vote"]] : @"";
        _favorite = [dictionary isValid:@"favorite"] ? [dictionary[@"favorite"] boolValue] : NO;;
        _nsfw = [dictionary isValid:@"nsfw"] ? [[NSString alloc] initWithString:dictionary[@"nsfw"]] : @"";
        _section = [dictionary isValid:@"section"] ? [[NSString alloc] initWithString:dictionary[@"section"]] : @"";
        _account_url = [dictionary isValid:@"account_url"] ? [[NSString alloc] initWithString:dictionary[@"account_url"]] : @"";
        _account_id = [dictionary isValid:@"account_id"] ? [[NSString alloc] initWithString:dictionary[@"account_id"]] : @"";
        _is_ad = [dictionary isValid:@"is_ad"] ? [dictionary[@"is_ad"] boolValue] : NO;;
        _in_most_viral = [dictionary isValid:@"in_most_viral"] ? [dictionary[@"in_most_viral"] boolValue] : NO;;
        _has_sound = [dictionary isValid:@"has_sound"] ? [dictionary[@"has_sound"] boolValue] : NO;;
        _tags = [dictionary isValid:@"tags"] ? [[NSArray alloc] initWithArray:dictionary[@"tags"]] : nil;
        _ad_type = [dictionary isValid:@"ad_type"] ? [dictionary[@"ad_type"] integerValue] : 0;
        _ad_url = [dictionary isValid:@"ad_url"] ? [[NSString alloc] initWithString:dictionary[@"ad_url"]] : @"";
        _edited = [dictionary isValid:@"edited"] ? [[NSString alloc] initWithString:dictionary[@"edited"]] : @"";
        _in_gallery = [dictionary isValid:@"in_gallery"] ? [dictionary[@"in_gallery"] boolValue] : NO;;
        _link = [dictionary isValid:@"link"] ? [[NSString alloc] initWithString:dictionary[@"link"]] : @"";
        _mp4_size = [dictionary isValid:@"mp4_size"] ? [dictionary[@"mp4_size"] integerValue] : 0;
        _mp4 = [dictionary isValid:@"mp4"] ? [[NSString alloc] initWithString:dictionary[@"mp4"]] : @"";
        _gifv = [dictionary isValid:@"gifv"] ? [[NSString alloc] initWithString:dictionary[@"gifv"]] : @"";
        _hls = [dictionary isValid:@"hls"] ? [[NSString alloc] initWithString:dictionary[@"hls"]] : @"";
        _processing = [dictionary isValid:@"processing"] ? [[NSDictionary alloc] initWithDictionary:dictionary[@"processing"]] : nil;
        _comment_count = [dictionary isValid:@"comment_count"] ? [[NSString alloc] initWithString:dictionary[@"comment_count"]] : @"";
        _favorite_count = [dictionary isValid:@"favorite_count"] ? [[NSString alloc] initWithString:dictionary[@"favorite_count"]] : @"";
        _ups = [dictionary isValid:@"ups"] ? [[NSString alloc] initWithString:dictionary[@"ups"]] : @"";
        _downs = [dictionary isValid:@"downs"] ? [[NSString alloc] initWithString:dictionary[@"downs"]] : @"";
        _points = [dictionary isValid:@"points"] ? [[NSString alloc] initWithString:dictionary[@"points"]] : @"";
        _score = [dictionary isValid:@"score"] ? [[NSString alloc] initWithString:dictionary[@"score"]] : @"";
    }
    
    return self;
}

@end
