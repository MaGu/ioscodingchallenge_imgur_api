//
//  NSDictionary+Addition.h
//  iOSCodingChallenge
//
//  Created by MaGu on 3/19/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImgurImageInfo : NSObject

@property (nonatomic, strong) NSURL *imageURL;
@property (nonatomic, strong) NSString *imageID;
@property (nonatomic, strong) NSString *title;

@end

NS_ASSUME_NONNULL_END
