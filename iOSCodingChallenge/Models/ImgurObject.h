//
//  ImgurObject.h
//  iOSCodingChallenge
//
//  Created by MaGu on 3/18/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ImgurAdConfig;
@class ImgurImageInfo;

NS_ASSUME_NONNULL_BEGIN

@interface ImgurObject : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *descriptionImgurImage;
@property (nonatomic, strong) NSString *datetime;
@property (nonatomic, strong) NSString *cover;
@property (nonatomic) NSInteger cover_width;
@property (nonatomic) NSInteger cover_height;
@property (nonatomic, strong) NSString *account_url;
@property (nonatomic, strong) NSString *account_id;
@property (nonatomic, strong) NSString *privacy;
@property (nonatomic, strong) NSString *layout;
@property (nonatomic) NSInteger views;
@property (nonatomic, strong) NSString *link;
@property (nonatomic) NSInteger ups;
@property (nonatomic) NSInteger downs;
@property (nonatomic) NSInteger points;
@property (nonatomic) NSInteger score;
@property (nonatomic) BOOL is_album;
@property (nonatomic, strong) NSString *vote;
@property (nonatomic) BOOL favorite;
@property (nonatomic) BOOL nsfw;
@property (nonatomic, strong) NSString *section;
@property (nonatomic) NSInteger comment_count;
@property (nonatomic) NSInteger favorite_count;
@property (nonatomic, strong) NSString *topic;
@property (nonatomic) NSInteger topic_id;
@property (nonatomic) NSInteger images_count;
@property (nonatomic) BOOL in_gallery;
@property (nonatomic) BOOL is_ad;
@property (nonatomic, strong) NSArray *tags;
@property (nonatomic) NSInteger ad_type;
@property (nonatomic, strong) NSString *ad_url;
@property (nonatomic) BOOL in_most_viral;
@property (nonatomic) BOOL include_album_ads;
@property (nonatomic, strong) NSArray *images;
@property (nonatomic, strong) ImgurAdConfig *ad_config;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
- (ImgurImageInfo *)getImageInfo;

@end

NS_ASSUME_NONNULL_END
