//
//  ImgurImage.h
//  iOSCodingChallenge
//
//  Created by MaGu on 3/18/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImgurImage : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *descriptionImage;
@property (nonatomic, strong) NSString *datetime;
@property (nonatomic, strong) NSString *type;
@property (nonatomic) BOOL animated;
@property (nonatomic) NSInteger width;
@property (nonatomic) NSInteger height;
@property (nonatomic) NSInteger size;
@property (nonatomic) NSInteger views;
@property (nonatomic, strong) NSString *bandwidth;
@property (nonatomic, strong) NSString *vote;
@property (nonatomic) BOOL favorite;
@property (nonatomic, strong) NSString *nsfw;
@property (nonatomic, strong) NSString *section;
@property (nonatomic, strong) NSString *account_url;
@property (nonatomic, strong) NSString *account_id;
@property (nonatomic) BOOL is_ad;
@property (nonatomic) BOOL in_most_viral;
@property (nonatomic) BOOL has_sound;
@property (nonatomic, strong) NSArray *tags;
@property (nonatomic) NSInteger ad_type;
@property (nonatomic, strong) NSString *ad_url;
@property (nonatomic, strong) NSString *edited;
@property (nonatomic) BOOL in_gallery;
@property (nonatomic, strong) NSString *link;
@property (nonatomic) NSInteger mp4_size;
@property (nonatomic, strong) NSString *mp4;
@property (nonatomic, strong) NSString *gifv;
@property (nonatomic, strong) NSString *hls;
@property (nonatomic, strong) NSDictionary *processing;
@property (nonatomic, strong) NSString *comment_count;
@property (nonatomic, strong) NSString *favorite_count;
@property (nonatomic, strong) NSString *ups;
@property (nonatomic, strong) NSString *downs;
@property (nonatomic, strong) NSString *points;
@property (nonatomic, strong) NSString *score;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END
