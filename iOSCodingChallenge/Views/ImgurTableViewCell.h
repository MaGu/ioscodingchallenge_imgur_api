//
//  ImgurTableViewCell.h
//  iOSCodingChallenge
//
//  Created by MaGu on 3/18/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ImgurObject;

NS_ASSUME_NONNULL_BEGIN

@interface ImgurTableViewCell : UITableViewCell

- (void)configureWithImgurObject:(ImgurObject *)imgur;

@end

NS_ASSUME_NONNULL_END
