//
//  DetailViewController.m
//  iOSCodingChallenge
//
//  Created by MaGu on 3/20/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import "DetailViewController.h"
#import "ImgurImageInfo.h"
#import "FileUtilities.h"
#import <AVKit/AVKit.h>

@interface DetailViewController ()

@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@end

@implementation DetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = _imageInfo.title;
    
    NSData *imageData = [FileUtilities loadDataImageWithID:_imageInfo.imageID];
    _imageView.image = [UIImage imageWithData:imageData];
    
    if ([_imageInfo.imageURL.absoluteString containsString:@".mp4"])
    {
        [self presentVideo];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)presentVideo
{
    AVPlayer *moviePlayer = [AVPlayer playerWithURL:_imageInfo.imageURL];
    AVPlayerViewController *moviePlayerVC = [[AVPlayerViewController alloc] init];
    [self presentViewController:moviePlayerVC animated:YES completion:nil];
    moviePlayerVC.player = moviePlayer;
    [moviePlayer play];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
