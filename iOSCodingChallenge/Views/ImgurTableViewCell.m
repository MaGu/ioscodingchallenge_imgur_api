//
//  ImgurTableViewCell.m
//  iOSCodingChallenge
//
//  Created by MaGu on 3/18/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import "ImgurTableViewCell.h"
#import "ImgurObject.h"
#import "ImgurImageInfo.h"
#import "Constants.h"

@interface ImgurTableViewCell ()

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblViews;
@property (nonatomic, weak) IBOutlet UILabel *lblPoints;
@property (nonatomic, weak) IBOutlet UIImageView *image;

@end

@implementation ImgurTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)configureWithImgurObject:(ImgurObject *)imgur
{
    _lblTitle.text = imgur.title;
    _lblViews.text = [NSString stringWithFormat:@"%d", (int)imgur.views];
    _lblPoints.text = [NSString stringWithFormat:@"%d", (int)imgur.points];
    
    
    ImgurImageInfo *imageInfo = [imgur getImageInfo];
    
    if (imageInfo != nil)
    {
        [_image loadImageFromURL:imageInfo.imageURL ID:imageInfo.imageID];
    }
}

@end
