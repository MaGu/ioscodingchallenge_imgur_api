//
//  DetailViewController.h
//  iOSCodingChallenge
//
//  Created by MaGu on 3/20/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ImgurImageInfo;

NS_ASSUME_NONNULL_BEGIN

@interface DetailViewController : UIViewController

@property (nonatomic, strong) ImgurImageInfo *imageInfo;

@end

NS_ASSUME_NONNULL_END
