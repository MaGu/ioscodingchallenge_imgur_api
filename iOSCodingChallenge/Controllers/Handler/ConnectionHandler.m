//
//  ConnectionHandler.m
//  iOSCodingChallenge
//
//  Created by MaGu on 3/18/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import "ConnectionHandler.h"
#import "Constants.h"

@interface ConnectionHandler () <NSURLSessionDelegate>

@property (nonatomic, strong) NSMutableURLRequest *urlRequest;
@property (nonatomic, strong) NSURLSession *session;

@end

@implementation ConnectionHandler


- (instancetype)initWithUrl:(NSString *)serverURL andHeader:(NSDictionary *)headerRequest
{
    if ((self = [super init]))
    {
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        [sessionConfiguration setTimeoutIntervalForRequest:30.0f];
        [sessionConfiguration setHTTPMaximumConnectionsPerHost:1];
        [sessionConfiguration setHTTPCookieStorage:[NSHTTPCookieStorage sharedHTTPCookieStorage]];
        
        NSMutableDictionary *header = [[NSMutableDictionary alloc] init];
        [header setObject:@"application/json" forKey:@"Content-Type"];
        if (headerRequest != nil)
        {
            _headerRequest = headerRequest;
            [header addEntriesFromDictionary:headerRequest];
        }
        
        [sessionConfiguration setHTTPAdditionalHeaders:header];
        _session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
        serverURL = [serverURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        
        NSURL *url = [NSURL URLWithString:serverURL];
        _urlRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0f];
        _urlRequest.allowsCellularAccess = YES;
    }
    return self;
}


- (void)startConnectionWithHTTPMethod:(NSString *)httpMethod andParams:(NSDictionary *)params
{
    if ([httpMethod isEqualToString:kHTTP_POST] ||
        [httpMethod isEqualToString:kHTTP_DELETE] ||
        [httpMethod isEqualToString:kHTTP_PUT])
    {
        [_urlRequest setHTTPMethod:httpMethod];
        NSError *error = nil;
        NSData *data = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
        if (!error)
        {
            NSURLSessionUploadTask *sessionTask = [_session uploadTaskWithRequest:_urlRequest fromData:data completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
            {
                /* Implement response */
            }];
            
            [sessionTask resume];
        }
    }
    else if ([httpMethod isEqualToString:kHTTP_GET])
    {
        NSURLSessionDataTask *sessionTask = [_session dataTaskWithRequest:_urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
        {
            if (error == nil && data != nil)
            {
                NSDictionary *serverResponse = [self getParsedResponse:data];
                NSString *errorMessage = [serverResponse objectForKey:@"success"];
                int errorCode = [[serverResponse objectForKey:@"status"] intValue];
                id dataResponse = serverResponse[@"data"];
                
                if (errorCode == kSUCCESS_CODE)
                    [self completionBlock](dataResponse, errorMessage, errorCode);
                else
                    [self completionBlock](nil, errorMessage, errorCode);
            }
            else
                [self completionBlock](nil, error.localizedDescription, 0);
        }];
        
        [sessionTask resume];
    }
}


- (NSDictionary *)getParsedResponse:(NSData *)response
{
    NSError *error;
    NSDictionary *parsedResponse = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&error];
    if (error)
    {
        NSLog(@"error: %@", [error localizedDescription]);
    }
    NSLog(@"-- ParserResponse -- \n %@", parsedResponse);
    return parsedResponse;
}

@end
