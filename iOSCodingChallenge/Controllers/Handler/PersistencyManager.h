//
//  PersistencyManager.h
//  iOSCodingChallenge
//
//  Created by MaGu on 3/18/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PersistencyManager : NSObject

- (void)getImagesWithName:(NSString *)nameSearch withPage:(int)page withCompletion:(void(^)(NSArray *, NSString *, int))completion;

@end


