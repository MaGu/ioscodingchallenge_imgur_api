//
//  PersistencyManager.m
//  iOSCodingChallenge
//
//  Created by MaGu on 3/18/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import "PersistencyManager.h"
#import "ConnectionHandler.h"
#import "Constants.h"
#import "ImgurObject.h"

@implementation PersistencyManager

- (void)getImagesWithName:(NSString *)nameSearch withPage:(int)page withCompletion:(void(^)(NSArray *, NSString *, int))completion
{
    NSDictionary *headerRequest = @{@"Authorization" : @"Client-ID 126701cd8332f32"};
    NSString *url = [NSString stringWithFormat:@"https://api.imgur.com/3/gallery/search/time/%d?q=%@", page, nameSearch];
    
    ConnectionHandler *connection = [[ConnectionHandler alloc] initWithUrl:url andHeader:headerRequest];
    [connection setCompletionBlock:^(id response, NSString *errorMessage, int errorCode) {
        
        if (errorCode == kSUCCESS_CODE)
        {
            NSArray *dataArray = [response isKindOfClass:[NSDictionary class]] ? @[response] : response;
            NSMutableArray *arrayImages = [[NSMutableArray alloc] init];
            
            for (NSDictionary *dictImage in dataArray)
            {
                ImgurObject *imgurObject = [[ImgurObject alloc] initWithDictionary:dictImage];
                [arrayImages addObject:imgurObject];
            }
            
            if (completion)
                completion(arrayImages, errorMessage, errorCode);
        }
    }];
    
    [connection startConnectionWithHTTPMethod:kHTTP_GET andParams:nil];
}

@end
