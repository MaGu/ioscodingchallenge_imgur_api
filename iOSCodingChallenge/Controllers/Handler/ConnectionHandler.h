//
//  ConnectionHandler.h
//  iOSCodingChallenge
//
//  Created by MaGu on 3/18/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConnectionHandler : NSObject
{
    NSDictionary *_headerRequest;
}

@property (nonatomic, copy) void (^completionBlock)(id response, NSString *errorMessage, int errorCode);

- (instancetype)initWithUrl:(NSString *)serverURL andHeader:(NSDictionary *)headerRequest;
- (void)startConnectionWithHTTPMethod:(NSString *)httpMethod andParams:(NSDictionary *)params;

@end


