//
//  FileUtilities.m
//  iOSCodingChallenge
//
//  Created by MaGu on 3/20/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import "FileUtilities.h"
#import "Constants.h"

@implementation FileUtilities

+ (NSString *)documentDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return paths[0];
}

+ (BOOL)createDirectory:(NSString *)directory
{
    BOOL success = YES;
    
    NSString *path = [[self documentDirectory] stringByAppendingPathComponent:directory];
    NSFileManager *fm = [NSFileManager defaultManager];
    if(![fm fileExistsAtPath:path])
    {
        NSError *error = nil;
        if(![fm createDirectoryAtPath:path withIntermediateDirectories:NO attributes:nil error:&error])
        {
            NSLog(@"The file can´t be created: %@", path);
        }
        else
        {
            success = NO;
        }
    }
    
    return success;
}

+ (NSString *)imageDirectory
{
    if ([self createDirectory:kImageDirectory])
    {
        NSString *path = [[self documentDirectory] stringByAppendingPathComponent:kImageDirectory];
        return path;
    }
    
    return nil;
}

+ (void)saveDataImage:(NSData *)dataImage name:(NSString *)name
{
    NSString *path = [self imageDirectory];
    if (path)
    {
        path = [path stringByAppendingFormat:@"/%@.img", name];
        if (![dataImage writeToFile:path atomically:YES])
        {
            NSLog(@"Can´t write file: %@", name);
        }
    }
}

+ (NSData *)loadDataImageWithID:(NSString *)ID
{
    NSData *dataImage = nil;
    NSString *path = [self imageDirectory];
    if (path)
    {
        path = [path stringByAppendingFormat:@"/%@.img", ID];
        dataImage = [[NSData alloc] initWithContentsOfFile:path];
    }
    
    return dataImage;
}

@end
