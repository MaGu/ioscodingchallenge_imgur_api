//
//  FileUtilities.h
//  iOSCodingChallenge
//
//  Created by MaGu on 3/20/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FileUtilities : NSObject

+ (NSString *)documentDirectory;
+ (void)saveDataImage:(NSData *)dataImage name:(NSString *)name;
+ (NSData *)loadDataImageWithID:(NSString *)ID;

@end

NS_ASSUME_NONNULL_END
