//
//  NSDictionary+Addition.h
//  iOSCodingChallenge
//
//  Created by MaGu on 3/19/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import "NSDictionary+Addition.h"

@implementation NSDictionary (Addition)

- (BOOL)isValid:(NSString *)key
{
    id obj = self[key];
    
    if (obj == nil || [obj isKindOfClass:[NSNull class]])
    {
        return NO;
    }
    
    return YES;
}

@end
