//
//  UIImageView+Network.m
//  iOSCodingChallenge
//
//  Created by MaGu on 3/20/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import "UIImageView+Network.h"
#import "FileUtilities.h"
#import <objc/runtime.h>

static char URL_KEY;

@implementation UIImageView(Network)

- (void)setLoadImage
{
    self.image = nil;
}

- (void)loadImageFromURL:(NSURL *)url ID:(NSString*)ID
{
    self.imageURL = url;
    [self setLoadImage];
    
    NSData *cachedData = [FileUtilities loadDataImageWithID:ID];
    if (cachedData)
    {
        self.imageURL = nil;
        self.image = [UIImage imageWithData:cachedData];
        if (self.image == NULL)
            self.image = [UIImage imageNamed:@"icon_play"];
        return;
    }
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *imageFromData = [UIImage imageWithData:data];
        
        [FileUtilities saveDataImage:data name:ID];
        
        if ([self.imageURL.absoluteString isEqualToString:url.absoluteString])
        {
            dispatch_sync(dispatch_get_main_queue(), ^{
                if (imageFromData)
                    self.image = imageFromData;
                else
                    self.image = [UIImage imageNamed:@"icon_play"];
            });
        }
        else { }
        self.imageURL = nil;
    });
}

- (void)setImageURL:(NSURL *)newImageURL
{
    objc_setAssociatedObject(self, &URL_KEY, newImageURL, OBJC_ASSOCIATION_COPY);
}

- (NSURL *)imageURL
{
    return objc_getAssociatedObject(self, &URL_KEY);
}

@end
