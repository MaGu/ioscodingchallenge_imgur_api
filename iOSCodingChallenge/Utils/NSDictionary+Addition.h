//
//  NSDictionary+Addition.h
//  iOSCodingChallenge
//
//  Created by MaGu on 3/19/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (Addition)

- (BOOL)isValid:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
