//
//  UITableView+Reusable.m
//  iOSCodingChallenge
//
//  Created by MaGu on 3/19/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import "UITableView+Reusable.h"

@implementation UITableView (Reusable)

- (id)dequeueReusableCellWithClass:(Class)aClass
{
    id cell = [self dequeueReusableCellWithIdentifier:NSStringFromClass(aClass)];
    if(cell == nil)
    {
        cell = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(aClass) owner:nil options:nil].firstObject;
    }
    return cell;
}

@end
