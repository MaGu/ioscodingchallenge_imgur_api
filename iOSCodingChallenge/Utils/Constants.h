//
//  Constants.h
//  iOSCodingChallenge
//
//  Created by MaGu on 3/18/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

// * -- Server Http Methods -- *
#define kHTTP_POST   @"POST"
#define kHTTP_GET    @"GET"
#define kHTTP_DELETE @"DELETE"
#define kHTTP_PUT    @"PUT"

#define kSUCCESS_CODE 200

#define kImageDirectory @"Images"

#import "NSDictionary+Addition.h"
#import "UITableView+Reusable.h"
#import "UIImageView+Network.h"

#endif /* Constants_h */

