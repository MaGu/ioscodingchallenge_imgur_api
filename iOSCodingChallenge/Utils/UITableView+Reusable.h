//
//  UITableView+Reusable.h
//  iOSCodingChallenge
//
//  Created by MaGu on 3/19/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITableView (Reusable)

-(id)dequeueReusableCellWithClass:(Class)aClass;

@end

NS_ASSUME_NONNULL_END
