//
//  UIImageView+Network.h
//  iOSCodingChallenge
//
//  Created by MaGu on 3/20/19.
//  Copyright © 2019 Personal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImageView (Network)

- (void)loadImageFromURL:(NSURL *)url ID:(NSString*)ID;

@end

NS_ASSUME_NONNULL_END
